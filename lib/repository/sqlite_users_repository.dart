import 'package:saturday_drawer_app/models/userModel.dart';
import 'package:saturday_drawer_app/providers/db_provider.dart';
import 'package:saturday_drawer_app/repository/users_repository.dart';

class SqliteUsersRepository implements UsersRepository{

  final provider = DBProvider.db;

  @override
  Future<List<UserModel>> listUsers() {
    // TODO: implement ListUsers
    return provider.listaUsers();
  }

  @override
  Future<void> addUser(UserModel user) {
    // TODO: implement addUser
    return provider.addUser(user);
  }

  @override
  Future<void> deleteUserById(int id) {
    // TODO: implement deleteUserById
    return provider.deleteUserById(id);
  }

  @override
  Future<void> updateUser(UserModel user) {
    // TODO: implement updateUser
    return provider.updateUser(user);
  }

}