import 'package:saturday_drawer_app/models/userModel.dart';

abstract class UsersRepository {
  Future<void> addUser(UserModel user);

  Future<void> deleteUserById(int id);

  Future<List<UserModel>> listUsers();

  Future<void> updateUser(UserModel user);
}
