import 'package:flutter/material.dart';
import 'package:saturday_drawer_app/blocs/users/users_bloc.dart';
import 'package:saturday_drawer_app/models/userModel.dart';
import 'package:saturday_drawer_app/providers/db_provider.dart';

class AddUserPage extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final usernameController = TextEditingController();
  final cityController = TextEditingController();

  final usersBloc = UsersBloc();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            TextFormField(
              controller: usernameController,
              validator: (value) {
                return value.isEmpty
                    ? 'El campo username no puede estar vacio'
                    : null;
              },
              decoration: InputDecoration(
                hintText: 'Insert username',
                icon: Icon(Icons.person),
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
            TextFormField(
              controller: cityController,
              validator: (value) {
                return value.isEmpty
                    ? 'El campo ciudad no puede estar vacio'
                    : null;
              },
              decoration: InputDecoration(
                hintText: 'Insert city',
                icon: Icon(Icons.location_city),
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
            RaisedButton(
              child: Text('Guardar'),
              onPressed: () {
                if (_formKey.currentState.validate()) {
//                  DBProvider.db
//                      .addUser(UserModel(nombre: usernameController.text));
                usersBloc.addUser(UserModel(nombre: usernameController.text));

                  final snackBar = SnackBar(
                    duration: Duration(milliseconds: 1200),
                    content: Text(
                        'El usuario ${usernameController.text} ha sido guardado'),
                    action: SnackBarAction(
                      label: 'Undo',
                      onPressed: () {
                        // Some code to undo the change.
                      },
                    ),
                  );
                  Scaffold.of(context).showSnackBar(snackBar);
                  _formKey.currentState?.reset();
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
