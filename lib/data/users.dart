library users;


final UsersDataSource usersG = new UsersDataSource._private();

class UsersDataSource {
  UsersDataSource._private(){getUsers();}


  List<Map<String, String>> getUsers() {
    return [
      {'nombre': 'Luis Machado', 'ciudad': 'Santiago de los Caballeros'},
      {'nombre': 'José Ventura', 'ciudad': 'Santo Domingo'},
      {'nombre': 'Luis Alvarado', 'ciudad': 'Santiago de los Caballeros'},
      {'nombre': 'Natasha Acevedo', 'ciudad': 'La Vega'},
      {'nombre': 'Luis Machado', 'ciudad': 'Santiago de los Caballeros'},
      {'nombre': 'José Ventura', 'ciudad': 'Santo Domingo'},
      {'nombre': 'Luis Alvarado', 'ciudad': 'Santiago de los Caballeros'},
      {'nombre': 'Natasha Acevedo', 'ciudad': 'La Vega'},
      {'nombre': 'Luis Machado', 'ciudad': 'Santiago de los Caballeros'},
      {'nombre': 'José Ventura', 'ciudad': 'Santo Domingo'},
      {'nombre': 'Luis Alvarado', 'ciudad': 'Santiago de los Caballeros'},
      {'nombre': 'Natasha Acevedo', 'ciudad': 'La Vega'},
      {'nombre': 'Luis Machado', 'ciudad': 'Santiago de los Caballeros'},
      {'nombre': 'José Ventura', 'ciudad': 'Santo Domingo'},
      {'nombre': 'Luis Alvarado', 'ciudad': 'Santiago de los Caballeros'},
      {'nombre': 'Natasha Acevedo', 'ciudad': 'La Vega'},
    ];
  }
}