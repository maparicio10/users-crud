import 'package:flutter/material.dart';
import 'package:saturday_drawer_app/pages/add_user_page.dart';
import 'package:saturday_drawer_app/pages/home_page.dart';
import 'package:saturday_drawer_app/pages/list_user_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
//      initialRoute: '/',
//      routes: {
//        '/': (context) => HomePage(),
//        '/add': (context) => AddUserPage(),
//        '/list': (context) => ListUserPage()
//      },
    );
  }
}