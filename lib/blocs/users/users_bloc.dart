import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:saturday_drawer_app/models/userModel.dart';
import 'package:saturday_drawer_app/providers/db_provider.dart';
import 'package:saturday_drawer_app/repository/sqlite_users_repository.dart';
import 'package:saturday_drawer_app/repository/users_repository.dart';

part 'users_event.dart';

part 'users_state.dart';

class UsersBloc {
  static final UsersBloc _singleton = UsersBloc._();

  final UsersRepository _usersRepository = SqliteUsersRepository();

  final _userBroadcast = StreamController<List<UserModel>>.broadcast();

  factory UsersBloc() {
    return _singleton;
  }

  UsersBloc._() {
    listUsers();
  }

  Stream<List<UserModel>> get userStream => _userBroadcast.stream;

  listUsers() async {
    _userBroadcast.sink.add(await DBProvider.db.listaUsers());
  }

  addUser(UserModel user) async {
    await _usersRepository.addUser(user);
    listUsers();
  }

  deleteUserById(int id) async {
    await _usersRepository.deleteUserById(id);
    listUsers();
  }

  updateUser(UserModel user) async {
    await _usersRepository.updateUser(user);
    listUsers();
  }

  dispose() {
    _userBroadcast?.close();
  }
}

//class UsersBloc extends Bloc<UsersEvent, UsersState> {
//  final UsersRepository _usersRepository;
//  StreamSubscription _usersSubscription;
//
//  UsersBloc({@required UsersRepository usersRepository})
//      : assert(usersRepository != null),
//        _usersRepository = usersRepository;
//
//  @override
//  UsersState get initialState => InitialUsersState();
//
//  @override
//  Stream<UsersState> mapEventToState(UsersEvent event) async* {
//    // TODO: Add your event logic
//  }
//
//  Stream<UsersState> _mapLoadUsersState() async*{
//    _usersSubscription?.cancel();
//    _usersSubscription = _usersRepository.ListUsers().asStream().listen((user)
//    =>add(UpdateUser(user)));
//  }
//}
