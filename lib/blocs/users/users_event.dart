part of 'users_bloc.dart';

@immutable
abstract class UsersEvent extends Equatable{
  const UsersEvent();

  @override
  List<Object> get props => [];
}

class LoadUsers extends UsersEvent{}

class AddUser extends UsersEvent{
  final UserModel user;

  const AddUser(this.user);

  @override
  List<Object> get props => [user];
}

class UpdateUser extends UsersEvent{
  final UserModel user;

  const UpdateUser(this.user);

  @override
  List<Object> get props => [user];
}

class DeleteUserById extends UsersEvent{
  final int id;

  const DeleteUserById(this.id);

  @override
  List<Object> get props => [id];
}