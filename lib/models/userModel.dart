class UserModel {
  int id;
  String nombre;

  UserModel({this.id, this.nombre});

  //Constructor
  UserModel.fromMap(Map<String, dynamic> map){
    id = map['id'];
    nombre = map['nombre'];
  }

  //Method
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'nombre': nombre,
    };
  }
}
