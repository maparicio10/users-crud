import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:saturday_drawer_app/models/userModel.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:path/path.dart';

class DBProvider {
  static Database _database;
  static final DBProvider db = DBProvider._private();

  DBProvider._private();

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await initDataBase();
    return _database;
  }

  initDataBase() async {
    Directory appDirectory = await
    getApplicationDocumentsDirectory();
    final String path = join(appDirectory.path, 'users.db');

    return await openDatabase(
      path,
      version: 1,
      onOpen: (db) {},
      onCreate: (Database db, int version) async {
        //https://old.sqliteonline.com/
        await db.execute(
            'CREATE TABLE users(id INTEGER PRIMARY KEY AUTOINCREMENT,'
                ' nombre VARCHAR NOT NULL)');
      },
    );
  }

  Future<int> addUser(UserModel user) async {
    final db = await database;

//    final userId = await db.rawInsert(
//        'INSERT INTO users (id,nombre) VALUES (${user.id},${user.nombre})');
//    final userId = await db.rawInsert(
//        'INSERT INTO users (id,nombre) VALUES (?,?)',[user.id,user.nombre]);

    final userId = await db.insert('users', user.toMap());
    print('Se agrego ${userId}');
    return userId;
  }

  Future<List<UserModel>> listaUsers() async {
    final db = await database;

    final results = await db.query('users');

    List<UserModel> users = results.isNotEmpty
        ? results.map((user) => UserModel.fromMap(user)).toList()
        : [];
    print('Listando ${users.length}');
    return users;
  }

  Future<UserModel> searchUserById(int id) async {
    final db = await database;

    final result = await db.query('users', where: 'id = ?',
        whereArgs: [id]);
    //final result = await db.execute('SELECT * FROM users WHERE id = $id');

    return result.isNotEmpty ? UserModel.fromMap(result.first) : null;
  }

  Future<int> updateUser(UserModel user) async {
    final db = await database;

    final result = await db
        .update('users', user.toMap(), where: 'id = ?', whereArgs: [user.id]);
    return result;
  }

  Future<int> deleteUserById(int id) async {
    final db = await database;

    final result = await db.delete('users', where: 'id = ?', whereArgs: [id]);
    return result;
  }

  Future<int> deleteAllUsers() async {
    final db = await database;

    final result = await db.delete('users');
    return result;
  }
}
